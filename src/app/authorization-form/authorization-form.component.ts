import { Component, Output, EventEmitter, Input } from '@angular/core';

import { ModelDataAuthorizations } from '../shared/models/form-data';
import { FormSettingsAuthorization } from '../shared/models/interfaces';

@Component({
  selector: 'app-authorization-form',
  templateUrl: './authorization-form.component.html',
  styleUrls: ['./authorization-form.component.scss']
})
export class AuthorizationFormComponent {
  @Input() public settingsForm: FormSettingsAuthorization;
  @Input() public authorizationForm: ModelDataAuthorizations;

  @Output() public newFormEvent: any = new EventEmitter<object>();

  public addAuthorizationForm(value: ModelDataAuthorizations): void {
    this.newFormEvent.emit(value);
  }
}
