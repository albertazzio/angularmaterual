export class ModelDataAuthorizations {
  login: string;
  password: string;
  email: string;
  location: string;
  isRememberMe: boolean = false;
}