import { FormSettingsAuthorization } from '../models/interfaces';

export const AUTHORIZATION_SETTINGS_HOME: FormSettingsAuthorization = {
    isLogin: true,
    isPassword: true,
    isEmail: true,
    isLocation: true,
 }

