import { Component } from '@angular/core';

import { ModelDataAuthorizations } from './shared/models/form-data';
import { AUTHORIZATION_SETTINGS_HOME } from './shared/constants/authorization-settings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public settingsForm = AUTHORIZATION_SETTINGS_HOME;
  public authorizationForm: ModelDataAuthorizations = new ModelDataAuthorizations();

  public addForm(newFormSettings: ModelDataAuthorizations): void {
    console.log(newFormSettings);
  }
}


